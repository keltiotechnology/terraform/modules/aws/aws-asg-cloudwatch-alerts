/* ----------------------------------------------------------------------------------------- */
/* Usage: Alert when autoscaling group is close to max instance and cpu + ram are close to max too */
/* Requirements:
 * - Need Cloudwatch agent installed on EC2 instance
   - Need custom memory metrics published with Autoscaling group as dimention
   - Visit https://gitlab.com/keltiotechnology/terraform/modules/aws/deprecated/auto_scaling_with_launch_template/-/tree/main/templates and check
   the file memory_metric_logs.json. Ensure you have it use memory metrics with Cloudwatch
 *
 * Metric Details
# We create a composite alarm which is composed of three alarms:
## - Autoscaling-group alarm which monitors the metric GroupInServiceInstances (The number of instances running in Autoscaling group)
## - CPU alarm which monitors the metric CPUUtilization
## - RAM alarm which monitors the metric mem_available_percent - a custom metric requires to explicitly customize the Cloudwatch agent configuration

# When all of three alarms are reaching their thresholds, the composite alarms is then triggered
/* ----------------------------------------------------------------------------------------- */
locals {
  ag_size_full_alarm_name    = "${var.namespace}/${var.stage}/${var.ag_size_full_alarm_name}"
  ag_cpu_overload_alarm_name = "${var.namespace}/${var.stage}/${var.ag_cpu_overload_alarm_name}"
  ag_ram_overload_alarm_name = "${var.namespace}/${var.stage}/${var.ag_ram_overload_alarm_name}"
  ag_overloading_alarm_name  = "${var.namespace}/${var.stage}/${var.ag_overloading_alarm_name}"
}

resource "aws_cloudwatch_metric_alarm" "ag_full" {
  alarm_name                = local.ag_size_full_alarm_name
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.ag_size_full_evaluation_periods
  metric_name               = "GroupInServiceInstances"
  namespace                 = "AWS/AutoScaling"
  period                    = var.ag_size_full_period
  statistic                 = "Minimum"
  threshold                 = var.ag_threshold_max_size
  alarm_description         = "This metric monitors number of running ec2 instance in AG"
  insufficient_data_actions = []

  dimensions = {
    AutoScalingGroupName = var.autoscaling_group_name
  }
}

resource "aws_cloudwatch_metric_alarm" "ag_cpu_full" {
  alarm_name                = local.ag_cpu_overload_alarm_name
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.ag_cpu_overload_evaluation_periods
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = var.ag_cpu_overload_period
  statistic                 = "Average"
  threshold                 = var.ag_cpu_overload_threshold
  alarm_description         = "This metric monitors CPU average utilization of ec2 instances in AG"
  insufficient_data_actions = []

  dimensions = {
    AutoScalingGroupName = var.autoscaling_group_name
  }
}

resource "aws_cloudwatch_metric_alarm" "ag_ram_full" {
  alarm_name                = local.ag_ram_overload_alarm_name
  comparison_operator       = "LessThanOrEqualToThreshold"
  evaluation_periods        = var.ag_ram_overload_evaluation_periods
  metric_name               = "mem_available_percent"
  namespace                 = var.cloudwatch_agent_namespace
  period                    = var.ag_ram_overload_period
  statistic                 = "Average"
  threshold                 = var.ag_ram_overload_threshold
  alarm_description         = "This metric monitors available RAM percent of ec2 instances in AG"
  insufficient_data_actions = []

  dimensions = {
    AutoScalingGroupName = var.autoscaling_group_name
  }
}

locals {
  alarm_rule_with_newlines = <<-EOF
ALARM("${local.ag_size_full_alarm_name}") AND ALARM("${local.ag_cpu_overload_alarm_name}") AND ALARM("${local.ag_ram_overload_alarm_name}")
EOF
}


resource "aws_cloudwatch_composite_alarm" "ag_overloading" {
  alarm_description = var.ag_overloading_alarm_description
  alarm_name        = local.ag_overloading_alarm_name

  alarm_rule = trimspace(replace(local.alarm_rule_with_newlines, "/\n+/", " "))

  alarm_actions = var.ag_overloading_alarm_actions

  depends_on = [
    aws_cloudwatch_metric_alarm.ag_full,
    aws_cloudwatch_metric_alarm.ag_cpu_full,
    aws_cloudwatch_metric_alarm.ag_ram_full,
  ]
}
/* ----------------------------------------------------------------------------------------- */
