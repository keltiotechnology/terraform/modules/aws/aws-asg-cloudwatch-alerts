output "alarm_ag_full_arn" {
  description = "ARN of the cloudwatch metric alarm to alert when autoscaling group is getting overloading"
  value       = aws_cloudwatch_metric_alarm.ag_full.arn
}

output "alarm_ag_cpu_full_arn" {
  description = "ARN of the cloudwatch metric alarm to alert when autoscaling group is getting overloading on CPU"
  value       = aws_cloudwatch_metric_alarm.ag_cpu_full.arn
}

output "alarm_ag_ram_full_arn" {
  description = "ARN of the cloudwatch metric alarm to alert when autoscaling group is getting overloading ON RAM"
  value       = aws_cloudwatch_metric_alarm.ag_ram_full.arn
}

output "alarm_ag_overloading_arn" {
  description = "ARN of the cloudwatch composite alarm to alert when autoscaling group is overloading"
  value       = aws_cloudwatch_composite_alarm.ag_overloading.arn
}
