/* ------------------------------------------------------------------------------------------------------------ */
/* Shared variables                                                                                             */
/* ------------------------------------------------------------------------------------------------------------ */
variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name"
  default     = ""
}

variable "stage" {
  type        = string
  description = "Stage, which can be 'prod', 'staging', 'dev'..."
  default     = ""
}
/* ------------------------------------------------------------------------------------------------------------ */


/* ------------------------------------------------------------------------------------------------------------ */
/* Alert: When autoscaling group is close to max instance and cpu + ram are close to max too */
/* ------------------------------------------------------------------------------------------------------------ */
variable "autoscaling_group_name" {
  type        = string
  description = "Autoscaling group name"
}

variable "cloudwatch_agent_namespace" {
  type        = string
  description = "Namespace of the Cloudwatch Agent custom metric"
  default     = "CWAgent"
}

variable "ag_overloading_alarm_name" {
  type        = string
  description = "Name of the Composite Alarm to alert Autoscaling group is getting overloading"
  default     = "AutoscalingGroup-Overload"
}

variable "ag_overloading_alarm_description" {
  type        = string
  description = "Description of the Composite Alarm to alert Autoscaling group is getting overloading"
  default     = "Alarm when the autoscaling group gets overloading"
}

variable "ag_size_full_alarm_name" {
  type        = string
  description = "Description of the Alarm to alert Autoscaling group is reaching maximum size"
  default     = "AutoscalingGroup-Size-Reach-Max"
}

variable "ag_cpu_overload_alarm_name" {
  type        = string
  description = "Description of the Alarm to alert CPU per Autoscaling group overloads"
  default     = "AutoscalingGroup-CPU-Overload"
}

variable "ag_ram_overload_alarm_name" {
  type        = string
  description = "Description of the Alarm to alert RAM per Autoscaling group overloads"
  default     = "AutoscalingGroup-RAM-Overload"
}

variable "ag_threshold_max_size" {
  type        = number
  description = "The threshold (count) used to alert that the autoscaling group has scaled to maximum allowed size"
  default     = 3
}

variable "ag_overloading_alarm_actions" {
  type        = list(string)
  description = "The list of actions, e.g. ARNs of SNS topic, to execute when this alarm transisions into ALARM state. Up to 5 actions are allowed."
  default     = []
}

variable "ag_size_full_evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold. Applied to the alert when Autoscaling group size is getting full"
  default     = 3
}

variable "ag_size_full_period" {
  type        = number
  description = "Period in second to query metrics used for alerts. Applied to the alert when Autoscaling group size is getting full"
  default     = 60
}

variable "ag_cpu_overload_evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold. Applied to the alert when Autoscaling group CPU is getting overload"
  default     = 3
}

variable "ag_cpu_overload_period" {
  type        = number
  description = "Period in second to query metrics used for alerts. Applied to the alert when Autoscaling group CPU is getting overload"
  default     = 60
}

variable "ag_cpu_overload_threshold" {
  type        = number
  description = "The threshold (percent) used to alert that the CPU per Autoscaling group overloads"
  default     = 80
}


variable "ag_ram_overload_threshold" {
  type        = number
  description = "The threshold (percent) used to alert that the RAM per Autoscaling group overloads"
  default     = 30
}

variable "ag_ram_overload_evaluation_periods" {
  type        = number
  description = "The threshold used to alert that the RAM per Autoscaling group overloads"
  default     = 3
}

variable "ag_ram_overload_period" {
  type        = number
  description = "Period in second to query metrics used for alerts. Applied to the alert when Autoscaling group RAM is getting overload"
  default     = 60
}
/* ------------------------------------------------------------------------------------------------------------ */
