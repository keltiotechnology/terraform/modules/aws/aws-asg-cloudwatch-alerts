# Autoscaling group alerts

## Requirements

- Need Cloudwatch agent installed on EC2 instance

## Usage

Create Alarm to alert when Autoscaling group is getting overloading:

```hcl-terraform
module "ags_cloudwath_alerts" {
  source                          = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-asg-cloudwatch-alerts"
  namespace                       = "devops"
  stage                           = "prod"
  autoscaling_group_name          = "<Autoscaling group name>"  
}
```

To subscribe to Alarm state, we need to provide a list of ARN SNS topic.

```hcl-terraform
module "sns_subscription_ag_full" {
  source                          = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-sns-email-subscription"
  sns_topic_name                  = "SNS_TOPIC_Autoscaling-group-overloading"
  user_emails                     = ["user@example.com"]
}

module "cloudfront_ag_overloading" {
  source                          = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-asg-cloudwatch-alerts"
  namespace                       = "devops"
  stage                           = "prod"  
  autoscaling_group_name          = var.autoscaling_group_name
  ag_overloading_alarm_actions    = [module.sns_subscription_ag_full.aws_sns_topic_arn]
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
- Need Cloudwatch agent installed on EC2 instance

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_composite_alarm.ag_overloading](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_composite_alarm) | resource |
| [aws_cloudwatch_metric_alarm.ag_cpu_full](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.ag_full](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.ag_ram_full](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ag_cpu_overload_alarm_name"></a> [ag\_cpu\_overload\_alarm\_name](#input\_ag\_cpu\_overload\_alarm\_name) | Description of the Alarm to alert CPU per Autoscaling group overloads | `string` | `"AutoscalingGroup-CPU-Overload"` | no |
| <a name="input_ag_cpu_overload_evaluation_periods"></a> [ag\_cpu\_overload\_evaluation\_periods](#input\_ag\_cpu\_overload\_evaluation\_periods) | The number of periods over which data is compared to the specified threshold. Applied to the alert when Autoscaling group CPU is getting overload | `number` | `3` | no |
| <a name="input_ag_cpu_overload_period"></a> [ag\_cpu\_overload\_period](#input\_ag\_cpu\_overload\_period) | Period in second to query metrics used for alerts. Applied to the alert when Autoscaling group CPU is getting overload | `number` | `60` | no |
| <a name="input_ag_cpu_overload_threshold"></a> [ag\_cpu\_overload\_threshold](#input\_ag\_cpu\_overload\_threshold) | The threshold (percent) used to alert that the CPU per Autoscaling group overloads | `number` | `80` | no |
| <a name="input_ag_overloading_alarm_actions"></a> [ag\_overloading\_alarm\_actions](#input\_ag\_overloading\_alarm\_actions) | The list of actions, e.g. ARNs of SNS topic, to execute when this alarm transisions into ALARM state. Up to 5 actions are allowed. | `list(string)` | `[]` | no |
| <a name="input_ag_overloading_alarm_description"></a> [ag\_overloading\_alarm\_description](#input\_ag\_overloading\_alarm\_description) | Description of the Composite Alarm to alert Autoscaling group is getting overloading | `string` | `"Alarm when the autoscaling group gets overloading"` | no |
| <a name="input_ag_overloading_alarm_name"></a> [ag\_overloading\_alarm\_name](#input\_ag\_overloading\_alarm\_name) | Name of the Composite Alarm to alert Autoscaling group is getting overloading | `string` | `"AutoscalingGroup-Overload"` | no |
| <a name="input_ag_ram_overload_alarm_name"></a> [ag\_ram\_overload\_alarm\_name](#input\_ag\_ram\_overload\_alarm\_name) | Description of the Alarm to alert RAM per Autoscaling group overloads | `string` | `"AutoscalingGroup-RAM-Overload"` | no |
| <a name="input_ag_ram_overload_evaluation_periods"></a> [ag\_ram\_overload\_evaluation\_periods](#input\_ag\_ram\_overload\_evaluation\_periods) | The threshold used to alert that the RAM per Autoscaling group overloads | `number` | `3` | no |
| <a name="input_ag_ram_overload_period"></a> [ag\_ram\_overload\_period](#input\_ag\_ram\_overload\_period) | Period in second to query metrics used for alerts. Applied to the alert when Autoscaling group RAM is getting overload | `number` | `60` | no |
| <a name="input_ag_ram_overload_threshold"></a> [ag\_ram\_overload\_threshold](#input\_ag\_ram\_overload\_threshold) | The threshold (percent) used to alert that the RAM per Autoscaling group overloads | `number` | `30` | no |
| <a name="input_ag_size_full_alarm_name"></a> [ag\_size\_full\_alarm\_name](#input\_ag\_size\_full\_alarm\_name) | Description of the Alarm to alert Autoscaling group is reaching maximum size | `string` | `"AutoscalingGroup-Size-Reach-Max"` | no |
| <a name="input_ag_size_full_evaluation_periods"></a> [ag\_size\_full\_evaluation\_periods](#input\_ag\_size\_full\_evaluation\_periods) | The number of periods over which data is compared to the specified threshold. Applied to the alert when Autoscaling group size is getting full | `number` | `3` | no |
| <a name="input_ag_size_full_period"></a> [ag\_size\_full\_period](#input\_ag\_size\_full\_period) | Period in second to query metrics used for alerts. Applied to the alert when Autoscaling group size is getting full | `number` | `60` | no |
| <a name="input_ag_threshold_max_size"></a> [ag\_threshold\_max\_size](#input\_ag\_threshold\_max\_size) | The threshold (count) used to alert that the autoscaling group has scaled to maximum allowed size | `number` | `3` | no |
| <a name="input_autoscaling_group_name"></a> [autoscaling\_group\_name](#input\_autoscaling\_group\_name) | Autoscaling group name | `string` | n/a | yes |
| <a name="input_cloudwatch_agent_namespace"></a> [cloudwatch\_agent\_namespace](#input\_cloudwatch\_agent\_namespace) | Namespace of the Cloudwatch Agent custom metric | `string` | `"CWAgent"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace, which could be your organization name | `string` | `""` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | Stage, which can be 'prod', 'staging', 'dev'... | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_alarm_ag_cpu_full_arn"></a> [alarm\_ag\_cpu\_full\_arn](#output\_alarm\_ag\_cpu\_full\_arn) | ARN of the cloudwatch metric alarm to alert when autoscaling group is getting overloading on CPU |
| <a name="output_alarm_ag_full_arn"></a> [alarm\_ag\_full\_arn](#output\_alarm\_ag\_full\_arn) | ARN of the cloudwatch metric alarm to alert when autoscaling group is getting overloading |
| <a name="output_alarm_ag_overloading_arn"></a> [alarm\_ag\_overloading\_arn](#output\_alarm\_ag\_overloading\_arn) | ARN of the cloudwatch composite alarm to alert when autoscaling group is overloading |
| <a name="output_alarm_ag_ram_full_arn"></a> [alarm\_ag\_ram\_full\_arn](#output\_alarm\_ag\_ram\_full\_arn) | ARN of the cloudwatch metric alarm to alert when autoscaling group is getting overloading ON RAM |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
